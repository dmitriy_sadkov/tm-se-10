# TM-SE-10
```
Task tm-se-10 I-TECO
```
## TECHNOLOGIES

* Maven 4.0
* Java SE 1.8
* Junit 4.11
* Jcabi 1.1
* Lombok 1.18.10
* Reflections 0.9.10

## DEVELOPER
```
Sadkov Dmitriy
dmitriy_sadkov@mail.ru
```
## BUILDING FROM SOURCE
```
-mvn clean install
```
## SOFTWARE REQUIREMENTS
```
-jdk 1.8
```

## USING THE PROJECT MANAGER
```
From the command-line
Download the project manager and run it with:
java -jar C:\Users\user\tm-se-09\target\tm-se-10-1.0.jar
```