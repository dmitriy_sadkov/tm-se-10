package ru.sadkov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.api.IUserService;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {

    private IUserRepository userRepository;
    private User currentUser;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
        this.currentUser = null;
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }

    public void userRegister(@Nullable final User user) throws NoSuchAlgorithmException {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        if (userRepository.findByLogin(user.getLogin()) != null) return;
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userRepository.persist(user);
    }

    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        userRepository.persist(user);
    }

    public boolean login(@Nullable final String login, @Nullable final String password) throws NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null || user.getPassword() == null) return false;
        if (user.getPassword().equals(HashUtil.hashMD5(password))) {
            currentUser = user;
            return true;
        }
        return false;
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void logout() {
        currentUser = null;
    }

    public void updatePassword(@Nullable final String newPassword) throws NoSuchAlgorithmException {
        if (newPassword == null || newPassword.isEmpty()) return;
        currentUser.setPassword(HashUtil.hashMD5(newPassword));
    }

    @Nullable
    public User findOne() {
        return currentUser;
    }

    public boolean updateProfile(@Nullable final String newUserName) {
        if (newUserName == null || newUserName.isEmpty()) return false;
        currentUser.setLogin(newUserName);
        return true;
    }

    public void addTestUsers() throws NoSuchAlgorithmException {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        clear();
        for (@NotNull final User user : users) {
            userRepository.persist(user);
        }
    }
}
