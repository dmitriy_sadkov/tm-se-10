package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IDomainService;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.domain.Domain;

public final class DomainService implements IDomainService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public DomainService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().load(domain.getUsers());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
    }

    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().findAll()));
        domain.setTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().findAll()));
    }
}
