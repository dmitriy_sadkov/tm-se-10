package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.repository.ProjectRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectService() {
    }

    @Override
    @Nullable
    public List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @NotNull final List<Project> projects = new ArrayList<>(projectRepository.findAll(userId));
        projects.sort(comparator);
        return projects;
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findProjectIdByName(projectName, userId);
    }

    public boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        return projectRepository.merge(projectName, userId, description);
    }

    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.remove(userId, projectName);
    }

    @Nullable
    public List<Project> findAll(@Nullable final User user) {
        if (user == null) return null;
        return new ArrayList<>(projectRepository.findAll(user.getId()));
    }

    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        projectRepository.removeAll(user.getId());
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(@NotNull final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void update(@Nullable final Project project, @Nullable final String projectName, @Nullable final String description) {
        if (project == null || projectName == null || projectName.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.update(project.getUserId(), project.getId(), projectName, description);
    }

    @Nullable
    public Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        @Nullable final String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOne(projectId, currentUser.getId());
    }

    @Override
    @Nullable
    public List<Project> findProjectsByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        return projectRepository.findProjectsByPart(userId, part);
    }

    @Override
    @Nullable
    public List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        return projectRepository.findProjectsByStatus(userId, status);
    }

    @Override
    @Nullable
    public String startProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final Date startDate = projectRepository.startProject(userId, projectName);
        if (startDate == null) return null;
        return simpleDateFormat.format(startDate);
    }

    @Override
    public @Nullable String endProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final Date endDate = projectRepository.endProject(userId, projectName);
        if (endDate == null) return null;
        return simpleDateFormat.format(endDate);
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final List<User> userList) {
        return projectRepository.findAll(userList);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.persist(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            projectRepository.persist(project);
        }
    }
}
