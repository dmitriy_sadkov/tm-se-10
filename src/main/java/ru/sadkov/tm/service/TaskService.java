package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.repository.TaskRepository;

import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private ITaskRepository taskRepository;
    @NotNull
    private IProjectService projectService;

    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectService projectService) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @NotNull
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Nullable
    public Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        for (@NotNull final Task task : tasks) {
            if (task.getName().equals(taskName)) {
                return task;
            }
        }
        return null;
    }

    @Nullable
    public List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        tasks.sort(comparator);
        return tasks;
    }

    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUser == null) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) {
            return false;
        }
        taskRepository.merge(taskName, projectId, currentUser.getId());
        return true;
    }

    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        if (currentUser == null) return;
        @Nullable final Task task = taskRepository.findTaskByName(taskName, currentUser.getId());
        if (task == null) return;
        taskRepository.removeByName(taskName, currentUser.getId());
    }

    @Nullable
    public List<Task> findAll(@Nullable final User user) {
        if (user == null) return null;
        return taskRepository.findAll(user.getId());
    }


    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        taskRepository.removeAll(user.getId());
    }

    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        @Nullable final Project project = projectService.getProjectRepository().findOne(projectId, currentUser.getId());
        if (project == null) return;
        if (project.getUserId().equals(currentUser.getId())) {
            @NotNull final Iterator<Task> iterator = taskRepository.findAll(currentUser.getId()).iterator();
            while (iterator.hasNext()) {
                @NotNull final Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskRepository.removeByName(task.getName(), currentUser.getId());
                }
            }
        }
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId) {
        if (oldName == null || oldName.isEmpty()) return;
        if (newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.update(oldName, newName, userId);
    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        return taskRepository.getTasksByPart(userId, part);
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        return taskRepository.findTasksByStatus(userId, status);
    }

    @Override
    @Nullable
    public String startTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @Nullable final Date startDate = taskRepository.startTask(userId, taskName);
        if (startDate == null) return null;
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Nullable
    public String endTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @Nullable final Date endDate = taskRepository.endTask(userId, taskName);
        if (endDate == null) return null;
        return simpleDateFormat.format(endDate);
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final List<User> userList) {
        return taskRepository.findAll(userList);
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.persist(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        for (@NotNull final Task task : tasks) {
            taskRepository.persist(task);
        }
    }
}

