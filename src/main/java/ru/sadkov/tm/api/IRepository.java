package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IRepository<T> {

    void persist(@NotNull T t);

    boolean isEmpty();

    void clear();
}
