package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public interface ServiceLocator {

    @Nullable
    Set<Class<? extends AbstractCommand>> getClasses();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    List<AbstractCommand> getCommandList();

    @NotNull
    Scanner getScanner();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

}
