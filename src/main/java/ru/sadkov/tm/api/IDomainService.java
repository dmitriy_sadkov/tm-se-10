package ru.sadkov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.domain.Domain;

public interface IDomainService {
    void load(@Nullable final Domain domain);

    void export(@Nullable final Domain domain);
}
