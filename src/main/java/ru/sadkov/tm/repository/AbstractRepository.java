package ru.sadkov.tm.repository;

import ru.sadkov.tm.api.IRepository;
import ru.sadkov.tm.entity.AbstractEntity;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public boolean isEmpty() {
        return entities.isEmpty();
    }
}
