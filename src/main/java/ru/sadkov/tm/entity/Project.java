package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Status;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "project")
public final class Project extends AbstractEntity implements Serializable {

    @NotNull
    private String name;
    @Nullable
    private String description;
    @NotNull
    private Date dateCreate;
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;
    @NotNull
    private String userId;
    @NotNull
    private Status status;

    public Project(@NotNull final String name, @NotNull final String id, @NotNull final String userId, final @NotNull String description) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    public Project(@NotNull final String name, @NotNull final String id, @NotNull final String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = "Description will be here";
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    @NotNull
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", id='" + id + '\'' +
                '}';
    }
}
