package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-removeByName";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null)
            throw new WrongDataException();
        serviceLocator.getTaskService().removeTask(taskName, serviceLocator.getUserService().getCurrentUser());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
