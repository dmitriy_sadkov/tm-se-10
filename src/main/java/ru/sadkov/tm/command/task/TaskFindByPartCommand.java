package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskFindByPartCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-find-by-part";
    }

    @Override
    public String description() {
        return "Find task by part of name or description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SEARCH]");
        if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException("[NO USER]");
        System.out.println("[ENTER PART OF NAME OR DESCRIPTION]");
        @Nullable final String part = serviceLocator.getScanner().nextLine();
        if (part == null || part.isEmpty()) throw new WrongDataException("INVALID NAME OR DESCRIPTION");
        @Nullable final List<Task> tasksList = serviceLocator.getTaskService()
                .getTasksByPart(serviceLocator.getUserService().getCurrentUser().getId(), part);
        ListShowUtil.showList(tasksList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
