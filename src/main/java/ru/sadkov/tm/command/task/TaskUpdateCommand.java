package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.exception.WrongDataException;

public final class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        @Nullable final String oldName = serviceLocator.getScanner().nextLine();
        if (oldName == null || oldName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null)
            throw new WrongDataException();
        @Nullable final Task task = serviceLocator.getTaskService().findTaskByName(oldName, serviceLocator.getUserService().getCurrentUser().getId());
        if (task == null) throw new WrongDataException("[NO SUCH TASK]");
        System.out.println("[ENTER NEW NAME]");
        @Nullable final String newName = serviceLocator.getScanner().nextLine();
        if (newName == null || newName.isEmpty()) throw new WrongDataException();
        serviceLocator.getTaskService().update(oldName, newName, serviceLocator.getUserService().getCurrentUser().getId());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
