package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty())
            throw new WrongDataException("[INCORRECT NAME]");
        boolean success = serviceLocator.getTaskService()
                .saveTask(taskName, projectName, serviceLocator.getUserService().getCurrentUser());
        if (!success) throw new WrongDataException("[FAILED]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
