package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all project";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        serviceLocator.getTaskService().removeAll(serviceLocator.getUserService().getCurrentUser());
        serviceLocator.getProjectService().removeAll(serviceLocator.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
