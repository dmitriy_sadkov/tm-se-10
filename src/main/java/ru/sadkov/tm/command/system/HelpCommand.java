package ru.sadkov.tm.command.system;

import ru.sadkov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command :
                serviceLocator.getCommandList()) {
            System.out.println(command.command() + ": "
                    + command.description());
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
