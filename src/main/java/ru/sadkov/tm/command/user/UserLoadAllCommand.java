package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.*;
import java.util.List;

public final class UserLoadAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load";
    }

    @Override
    public String description() {
        return "Load domen";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM FILE]");
        try (@NotNull final FileInputStream userFis = new FileInputStream("src/main/domen/userFile");
             @NotNull final FileInputStream projectFis = new FileInputStream("src/main/domen/projectFile");
             @NotNull final FileInputStream taskFis = new FileInputStream("src/main/domen/taskFile");
             @NotNull final ObjectInputStream userOis = new ObjectInputStream(userFis);
             @NotNull final ObjectInputStream projectOis = new ObjectInputStream(projectFis);
             @NotNull final ObjectInputStream taskOis = new ObjectInputStream(taskFis)) {
            serviceLocator.getUserService().clear();
            serviceLocator.getProjectService().clear();
            serviceLocator.getTaskService().clear();
            @NotNull final List<User> userList = (List<User>) userOis.readObject();
            if (userList.isEmpty()) throw new WrongDataException("[NO USERS]");
            for (@NotNull final User user : userList) {
                serviceLocator.getUserService().userAddFromData(user);
            }
            @NotNull final List<Project> projects = (List<Project>) projectOis.readObject();
            for (@NotNull final Project project : projects) {
                serviceLocator.getProjectService().persist(project);
            }
            @NotNull final List<Task> tasks = (List<Task>) taskOis.readObject();
            for (@NotNull final Task task : tasks) {
                serviceLocator.getTaskService().persist(task);
            }
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
