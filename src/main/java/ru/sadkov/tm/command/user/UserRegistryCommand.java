package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.HashUtil;

public final class UserRegistryCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-add";
    }

    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = serviceLocator.getScanner().nextLine();
        if(login==null||login.isEmpty()||password==null||password.isEmpty())throw new WrongDataException();
        @NotNull final User user = new User(login, HashUtil.hashMD5(password), Role.USER);
        serviceLocator.getUserService().userRegister(user);
        System.out.println("[USER ADDED]");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
