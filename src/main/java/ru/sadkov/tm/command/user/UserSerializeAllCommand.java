package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public final class UserSerializeAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "serialize";
    }

    @Override
    public String description() {
        return "Serialize domen";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        try (@NotNull final FileOutputStream userFos = new FileOutputStream("src/main/domen/userFile");
             @NotNull final FileOutputStream projectFOS = new FileOutputStream("src/main/domen/projectFile");
             @NotNull final FileOutputStream taskFos = new FileOutputStream("src/main/domen/taskFile");
             @NotNull final ObjectOutputStream userOos = new ObjectOutputStream(userFos);
             @NotNull final ObjectOutputStream projectOos = new ObjectOutputStream(projectFOS);
             @NotNull final ObjectOutputStream taskOos = new ObjectOutputStream(taskFos)) {
            if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException("[NO USER]");
            @NotNull final List<User> userList = getServiceLocator().getUserService().findAll();
            if (userList.isEmpty()) throw new WrongDataException("[NO USERS]");
            userOos.writeObject(userList);
            projectOos.writeObject(serviceLocator.getProjectService().findAll(userList));
            taskOos.writeObject(serviceLocator.getTaskService().findAll(userList));
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
