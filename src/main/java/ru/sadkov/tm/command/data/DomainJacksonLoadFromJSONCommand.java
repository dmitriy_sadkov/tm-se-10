package ru.sadkov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.File;

public final class DomainJacksonLoadFromJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-jackson-JSON";
    }

    @Override
    public String description() {
        return "Load domain from JSON with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM JSON]");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readerFor(Domain.class).readValue(new File("src/main/domen/domain.json"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public boolean safe() {
        return true;
    }
}
