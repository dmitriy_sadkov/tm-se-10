package ru.sadkov.tm.command.data;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DomainLoadFromJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-JSON";
    }

    @Override
    public String description() {
        return "Load domain from in JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM JSON]");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(new File("src/main/domen/domainFileJSON.json"));
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public boolean safe() {
        return true;
    }
}
