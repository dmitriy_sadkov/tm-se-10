package ru.sadkov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.FileOutputStream;

public final class DomainSaveJacksonXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-jackson-XML";
    }

    @Override
    public String description() {
        return "Save domain in XML with Jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        try(FileOutputStream fileOutputStream = new FileOutputStream("src/main/domen/domain.xml")){
            @NotNull final Domain domain = new Domain();
            serviceLocator.getDomainService().export(domain);
            @NotNull final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
            @Nullable final String domainString = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if(domainString==null||domainString.isEmpty())throw new WrongDataException("ERROR");
            fileOutputStream.write(domainString.getBytes());
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
