package ru.sadkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DomainLoadFromXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-XML";
    }

    @Override
    public String description() {
        return "Load domain from in XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM XML]");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(new File("src/main/domen/domainFile.xml"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public boolean safe() {
        return true;
    }
}
