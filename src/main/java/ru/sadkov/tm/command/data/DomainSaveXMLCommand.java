package ru.sadkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DomainSaveXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-XML";
    }

    @Override
    public String description() {
        return "Save domain in XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        marshaller.marshal(domain, new File("src/main/domen/domainFile.xml"));
    }

    @Override
    public boolean safe() {
        return true;
    }
}
