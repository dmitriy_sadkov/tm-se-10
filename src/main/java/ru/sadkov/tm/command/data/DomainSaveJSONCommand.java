package ru.sadkov.tm.command.data;


import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;


public final class DomainSaveJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-JSON";
    }

    @Override
    public String description() {
        return "Save domain in JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        marshaller.marshal(domain, new File("src/main/domen/domainFileJSON.json"));
    }

    @Override
    public boolean safe() {
        return true;
    }
}
