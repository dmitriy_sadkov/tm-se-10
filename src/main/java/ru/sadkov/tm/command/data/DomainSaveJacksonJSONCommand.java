package ru.sadkov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.FileOutputStream;

public final class DomainSaveJacksonJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-jackson-JSON";
    }

    @Override
    public String description() {
        return "Save domain in JSON with Jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/domen/domain.json")) {
            @NotNull final Domain domain = new Domain();
            serviceLocator.getDomainService().export(domain);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @Nullable final String domainString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if (domainString == null || domainString.isEmpty()) throw new WrongDataException("ERROR");
            fileOutputStream.write(domainString.getBytes());
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
