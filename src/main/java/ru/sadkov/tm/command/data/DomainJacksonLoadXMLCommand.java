package ru.sadkov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;

import java.io.File;

public final class DomainJacksonLoadXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-jackson-XML";
    }

    @Override
    public String description() {
        return "Load domain from XML with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM XML]");
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        @Nullable final Domain domain = xmlMapper.readerFor(Domain.class).readValue(new File("src/main/domen/domain.xml"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public boolean safe() {
        return true;
    }
}
