package ru.sadkov.tm.launcher;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.sadkov.tm.api.*;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.service.DomainService;
import ru.sadkov.tm.service.ProjectService;
import ru.sadkov.tm.service.TaskService;
import ru.sadkov.tm.service.UserService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository,projectService);
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository);
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
    @Nullable
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.sadkov.tm.command").getSubTypesOf(AbstractCommand.class);

    @Nullable
    public Set<Class<? extends AbstractCommand>> getClasses() {
        return classes;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandMap.values());
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws IllegalAccessException, InstantiationException {
        @Nullable final AbstractCommand abstractCommand = clazz.newInstance();
        if (abstractCommand == null) return;
        @Nullable final String commandName = abstractCommand.command();
        @Nullable final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        abstractCommand.setServiceLocator(this);
        commandMap.put(commandName, abstractCommand);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            try {
                execute(command);
                System.out.println("[DONE]");
                System.out.println("-----------------------------------");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("-----------------------------------");
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;

        if (abstractCommand.safe() || (!abstractCommand.safe() && userService.isAuth())) {
            abstractCommand.execute();
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    public void init() {
        if (classes == null || classes.isEmpty()) return;
        try {
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
                registry(clazz);
            }
            userService.addTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
