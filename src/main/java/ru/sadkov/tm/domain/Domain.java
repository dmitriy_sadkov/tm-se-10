package ru.sadkov.tm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlType
@XmlRootElement(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain {

    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @NotNull
    private List<Project> project = new ArrayList<>();

    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @NotNull
    private List<Task> task = new ArrayList<>();

    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @NotNull
    private List<User> user = new ArrayList<>();

    @NotNull
    public List<Project> getProjects() {
        return project;
    }

    public void setProjects(@NotNull final List<Project> projects) {
        this.project = projects;
    }

    @NotNull
    public List<Task> getTasks() {
        return task;
    }

    public void setTasks(@NotNull final List<Task> tasks) {
        this.task = tasks;
    }

    @NotNull
    public List<User> getUsers() {
        return user;
    }

    public void setUsers(@NotNull final List<User> users) {
        this.user = users;
    }
}
